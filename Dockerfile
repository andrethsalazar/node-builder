FROM node:alpine

ENV BUILD_APP_PATH="."

WORKDIR $BUILD_APP_PATH
ADD . $BUILD_APP_PATH

RUN yarn install
RUN npm run grunt